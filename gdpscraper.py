import requests
from bs4 import BeautifulSoup  
import csv

#Requests webpage content
result = requests.get('https://www.worldometers.info/gdp/gdp-by-country/')

#Save content in variable
src = result.content

#Soupactivate
soup = BeautifulSoup(src,'lxml')

#Look for table and save CSV
table = soup.find('table')
with open('gdp.csv', 'w', newline ='') as f:
    writer = csv.writer(f)
    for tr in table('tr'):
        row = [t.get_text(strip=True) for t in tr (['td', 'th'])]
        cell1 = row[0]
        cell2 = row[1]
        cell3 = row[2].replace(',','').replace('$','')
        row = [cell1, cell2,cell3]
        writer.writerow(row)
