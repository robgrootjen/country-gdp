The data set shows GDP in 2017 from every country

## Data
The data is sourced from: https://www.worldometers.info/gdp/gdp-by-country/

The repo includes:
* datapackage.json
* gdp.csv
* gdpscraper.py

## Preparation

***Requires:***
1. Python
2. Visualstudio / Jupyter notebook / or any platform that works with python.

***gdp.csv***
* The CSV Data has 3 colummns. The first column shows "#", the second column shows "Country" and the last column shows the GDP in 2017.

***datapackage.json***
* The json file has more detailed information such as name, licence, a graph integrated.

***gdpscraper.py***
* This script will scrape the table everytime you need to scrape. 

***Instructions:***
* Copy script
* Open in jupyter notebook or python shell.
* Run
* Csv file will be saved in your main document.

## Licence
This Data Package is made available under the Public Domain Dedication and License v1.0 